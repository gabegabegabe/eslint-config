import json from 'eslint-plugin-json';

export default [
	json.configs['recommended-with-comments']
];
