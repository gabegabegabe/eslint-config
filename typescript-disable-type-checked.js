import { DISABLED } from './lib/eslint-enforcement-codes.js';
import ts from 'typescript-eslint';

export default [
	ts.configs.disableTypeChecked,
	{
		rules: {
			'@typescript-eslint/naming-convention': DISABLED,
			'@typescript-eslint/no-unnecessary-qualifier': DISABLED,
			'@typescript-eslint/prefer-readonly': DISABLED,
			'@typescript-eslint/prefer-readonly-parameter-types': DISABLED,
			'@typescript-eslint/promise-function-async': DISABLED,
			'@typescript-eslint/require-array-sort-compare': DISABLED,
			'@typescript-eslint/strict-boolean-expressions': DISABLED,
			'@typescript-eslint/switch-exhaustiveness-check': DISABLED
		}
	}
];
