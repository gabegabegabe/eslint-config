import { baseRules } from './lib/base-rules.js';
import vue from 'eslint-plugin-vue';
import {
	DISABLED,
	ERROR,
	WARN
} from './lib/eslint-enforcement-codes.js';

const extendedRules = Object.entries(baseRules)
	.filter(([key]) => key in vue.rules)
	.reduce((rules, [key, configuration]) => ({
		...rules,
		[key]: DISABLED,
		[`vue/${key}`]: configuration
	}), {});

export default [
	...vue.configs['flat/recommended'],
	{
		rules: {
			// Priority A
			'vue/multi-word-component-names': DISABLED,

			// Priority B
			'vue/html-indent': [ERROR, 'tab'],
			'vue/html-self-closing': [
				ERROR,
				{
					html: { normal: 'never', void: 'always' }
				}
			],
			'vue/max-attributes-per-line': [
				ERROR,
				{
					singleline: 3,
					multiline: 3
				}
			],

			// Uncategorized
			'vue/block-lang': [
				ERROR,
				{
					script: { lang: 'ts' },
					style: { lang: 'scss' }
				}
			],
			'vue/block-order': [
				ERROR,
				{ order: ['template', 'script', 'style'] }
			],
			'vue/block-tag-newline': ERROR,
			'vue/component-api-style': [
				ERROR,
				['script-setup', 'composition']
			],
			'vue/component-name-in-template-casing': ERROR,
			'vue/component-options-name-casing': [ERROR, 'PascalCase'],
			'vue/custom-event-name-casing': [ERROR, 'kebab-case'],
			'vue/define-emits-declaration': [ERROR, 'type-based'],
			'vue/define-macros-order': [
				ERROR,
				{ order: ['defineProps', 'defineEmits'] }
			],
			'vue/define-props-declaration': [ERROR, 'type-based'],
			'vue/html-button-has-type': ERROR,
			'vue/html-comment-content-newline': ERROR,
			'vue/html-comment-content-spacing': ERROR,
			'vue/html-comment-indent': [ERROR, 'tab'],
			'vue/match-component-file-name': ERROR,
			'vue/match-component-import-name': ERROR,
			'vue/new-line-between-multi-line-property': ERROR,
			'vue/next-tick-style': ERROR,
			'vue/no-boolean-default': ERROR,
			'vue/no-deprecated-model-definition': ERROR,
			'vue/no-duplicate-attr-inheritance': WARN,
			'vue/no-empty-component-block': ERROR,
			'vue/no-multiple-objects-in-class': ERROR,
			'vue/no-potential-component-option-typo': ERROR,
			'vue/no-ref-object-reactivity-loss': ERROR,
			'vue/no-required-prop-with-default': ERROR,
			'vue/no-root-v-if': ERROR,
			'vue/no-setup-props-reactivity-loss': ERROR,
			'vue/no-static-inline-styles': [ERROR, { allowBinding: true }],
			'vue/no-template-target-blank': ERROR,
			'vue/no-this-in-before-route-enter': ERROR,
			'vue/no-undef-components': [
				ERROR,
				{
					ignorePatterns: [
						'router-link',
						'router-view'
					]
				}
			],
			'vue/no-undef-properties': ERROR,
			'vue/no-unused-emit-declarations': WARN,
			'vue/no-unused-refs': WARN,
			'vue/no-use-v-else-with-v-for': ERROR,
			'vue/no-useless-mustaches': [ERROR, { ignoreIncludesComment: true }],
			'vue/no-useless-v-bind': [ERROR, { ignoreIncludesComment: true }],
			'vue/no-v-text': ERROR,
			'vue/padding-line-between-blocks': ERROR,
			'vue/prefer-define-options': ERROR,
			'vue/prefer-prop-type-boolean-first': ERROR,
			'vue/prefer-separate-static-class': ERROR,
			'vue/prefer-true-attribute-shorthand': [ERROR, 'always'],
			'vue/require-direct-export': ERROR,
			'vue/require-emit-validator': ERROR,
			'vue/require-explicit-slots': ERROR,
			'vue/require-expose': ERROR,
			'vue/require-macro-variable-name': ERROR,
			'vue/require-name-property': ERROR,
			'vue/script-indent': [
				ERROR,
				'tab',
				{
					baseIndent: 1,
					switchCase: 1
				}
			],
			'vue/sort-keys': [
				ERROR,
				'asc',
				{ natural: true }
			],
			'vue/static-class-names-order': ERROR,
			'vue/v-for-delimiter-style': [ERROR, 'of'],
			'vue/v-on-handler-style': [
				ERROR,
				['method', 'inline']
			],
			'vue/valid-define-options': ERROR,

			// Extension Rules
			...extendedRules,
			'vue/no-constant-condition': ERROR,
			'vue/no-empty-pattern': ERROR,
			'vue/no-irregular-whitespace': ERROR,
			'vue/no-loss-of-precision': ERROR,
			'vue/no-sparse-arrays': ERROR
		}
	}
];
