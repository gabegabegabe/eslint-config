import { baseRules } from './lib/base-rules.js';
import ts from 'typescript-eslint';
import {
	DISABLED,
	ERROR
} from './lib/eslint-enforcement-codes.js';

const extendedRules = Object.entries(baseRules)
	.filter(([key]) => key in ts.plugin.rules)
	.reduce((rules, [key, configuration]) => ({
		...rules,
		[key]: DISABLED,
		[`@typescript-eslint/${key}`]: configuration
	}), {});

export default [
	...ts.configs.strictTypeChecked,
	...ts.configs.stylisticTypeChecked,
	{
		rules: {
			// Extend some base rules
			...extendedRules,

			// TypeScript Rules
			'@typescript-eslint/consistent-type-assertions': [
				ERROR,
				{
					assertionStyle: 'as',
					objectLiteralTypeAssertions: 'allow-as-parameter'
				}
			],
			'@typescript-eslint/consistent-type-definitions': [ERROR, 'type'],
			'@typescript-eslint/consistent-type-exports': ERROR,
			'@typescript-eslint/consistent-type-imports': ERROR,
			'@typescript-eslint/explicit-function-return-type': ERROR,
			'@typescript-eslint/explicit-member-accessibility': [ERROR, { accessibility: 'no-public' }],
			'@typescript-eslint/explicit-module-boundary-types': ERROR,
			'@typescript-eslint/member-ordering': ERROR,
			'@typescript-eslint/method-signature-style': ERROR,
			'@typescript-eslint/naming-convention': [
				ERROR,
				{
					selector: ['default', 'variable'],
					format: ['camelCase', 'PascalCase', 'UPPER_CASE'],
					leadingUnderscore: 'allow'
				}
			],
			'@typescript-eslint/no-extraneous-class': [
				ERROR,
				{
					allowEmpty: false,
					allowConstructorOnly: true,
					allowStaticOnly: false,
					allowWithDecorator: true
				}
			],
			'@typescript-eslint/no-import-type-side-effects': ERROR,
			'@typescript-eslint/no-unnecessary-qualifier': ERROR,
			'@typescript-eslint/no-useless-empty-export': ERROR,
			'@typescript-eslint/parameter-properties': ERROR,
			'@typescript-eslint/prefer-enum-initializers': ERROR,
			'@typescript-eslint/prefer-readonly': ERROR,
			'@typescript-eslint/prefer-readonly-parameter-types': [
				ERROR,
				{ treatMethodsAsReadonly: true }
			],
			'@typescript-eslint/promise-function-async': ERROR,
			'@typescript-eslint/require-array-sort-compare': ERROR,
			'@typescript-eslint/strict-boolean-expressions': ERROR,
			'@typescript-eslint/switch-exhaustiveness-check': ERROR,
			'@typescript-eslint/typedef': DISABLED
		}
	}
];
