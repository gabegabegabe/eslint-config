import base from './index.js';
import { DISABLED } from './lib/eslint-enforcement-codes.js';
import js from './javascript.js';
import json from './json.js';
import node from './node.js';

export default [
	...base,
	...node,
	...js,
	...json,
	{
		rules: {
			'no-magic-numbers': DISABLED,
			'sort-keys': DISABLED
		}
	}
];
