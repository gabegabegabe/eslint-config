import ts from 'typescript-eslint';
import {
	DISABLED,
	ERROR
} from './lib/eslint-enforcement-codes.js';

export default [
	{
		languageOptions: {
			parserOptions: {
				extraFileExtensions: ['.vue'],
				parser: ts.parser,
			}
		},
		rules: {
			// Disable some inherited rules
			'@typescript-eslint/indent': DISABLED,
			'@typescript-eslint/no-unused-vars': DISABLED,

			// Enable some additional Vue rules
			'vue/require-typed-object-prop': ERROR,
			'vue/require-typed-ref': ERROR
		}
	}
];
