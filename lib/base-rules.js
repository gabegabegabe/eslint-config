import {
	DISABLED,
	ERROR,
	WARN
} from './eslint-enforcement-codes.js';

export const baseRules = {
	// Eslint Rules
	// // Possible Problems
	'array-callback-return': ERROR,
	'no-await-in-loop': ERROR,
	'no-constructor-return': ERROR,
	'no-duplicate-imports': [ERROR, { includeExports: true }],
	'no-inner-declarations': ERROR,
	'no-promise-executor-return': ERROR,
	'no-self-compare': ERROR,
	'no-template-curly-in-string': ERROR,
	'no-unmodified-loop-condition': ERROR,
	'no-unreachable-loop': ERROR,
	'no-use-before-define': ERROR,
	'no-useless-assignment': ERROR,
	'require-atomic-updates': ERROR,

	// // Suggestions
	'accessor-pairs': [
		ERROR,
		{
			setWithoutGet: true,
			enforceForClassMembers: true
		}
	],
	'arrow-body-style': ERROR,
	'block-scoped-var': ERROR,
	camelcase: [
		WARN,
		{
			ignoreDestructuring: true,
			ignoreImports: true,
			properties: 'never'
		}
	],
	'capitalized-comments': [
		ERROR,
		'always',
		{
			ignoreConsecutiveComments: true
		}
	],
	'class-methods-use-this': ERROR,
	complexity: [ERROR, 10],
	'consistent-return': ERROR,
	'consistent-this': [ERROR, 'that'],
	curly: [ERROR, 'multi', 'consistent'],
	'default-case': ERROR,
	'default-case-last': ERROR,
	'default-param-last': ERROR,
	'dot-notation': ERROR,
	eqeqeq: [ERROR, 'smart'],
	'func-name-matching': [ERROR, 'always'],
	'func-names': WARN,
	'func-style': ERROR,
	'grouped-accessor-pairs': [ERROR, 'getBeforeSet'],
	'guard-for-in': WARN,
	'id-denylist': DISABLED,
	'id-length': [ERROR, { min: 2 }],
	'id-match': DISABLED,
	'init-declarations': [ERROR, 'always'],
	'logical-assignment-operators': [
		ERROR,
		'always',
		{ enforceForIfStatements: true }
	],
	'max-classes-per-file': [ERROR, 1],
	'max-depth': [ERROR, 4],
	'max-lines': [
		ERROR,
		{
			max: 750,
			skipBlankLines: true,
			skipComments: true
		}
	],
	'max-lines-per-function': [
		ERROR,
		{
			max: 100,
			skipBlankLines: true,
			skipComments: true,
			IIFEs: true
		}
	],
	'max-nested-callbacks': [ERROR, 3],
	'max-params': [ERROR, { max: 5 }],
	'max-statements': [ERROR, { max: 20 }],
	'new-cap': ERROR,
	'no-alert': ERROR,
	'no-array-constructor': ERROR,
	'no-bitwise': WARN,
	'no-caller': ERROR,
	'no-console': WARN,
	'no-continue': ERROR,
	'no-div-regex': ERROR,
	'no-else-return': [ERROR, { allowElseIf: false }],
	'no-empty-function': ERROR,
	'no-eq-null': ERROR,
	'no-eval': ERROR,
	'no-extend-native': ERROR,
	'no-extra-bind': ERROR,
	'no-extra-label': ERROR,
	'no-implicit-coercion': ERROR,
	'no-implicit-globals': ERROR,
	'no-implied-eval': ERROR,
	'no-inline-comments': DISABLED,
	'no-invalid-this': ERROR,
	'no-iterator': ERROR,
	'no-label-var': ERROR,
	'no-labels': ERROR,
	'no-lone-blocks': ERROR,
	'no-lonely-if': ERROR,
	'no-loop-func': ERROR,
	'no-magic-numbers': [ERROR, { ignore: [-1, 0, 1] }],
	'no-multi-assign': ERROR,
	'no-multi-str': ERROR,
	'no-negated-condition': ERROR,
	'no-nested-ternary': ERROR,
	'no-new': ERROR,
	'no-new-func': ERROR,
	'no-new-wrappers': ERROR,
	'no-object-constructor': ERROR,
	'no-octal-escape': ERROR,
	'no-param-reassign': ERROR,
	'no-plusplus': [WARN, { allowForLoopAfterthoughts: true }],
	'no-proto': ERROR,
	'no-restricted-exports': DISABLED,
	'no-restricted-globals': DISABLED,
	'no-restricted-imports': DISABLED,
	'no-restricted-properties': DISABLED,
	'no-restricted-syntax': DISABLED,
	'no-return-assign': ERROR,
	'no-script-url': ERROR,
	'no-sequences': ERROR,
	'no-shadow': [
		ERROR,
		{
			builtinGlobals: false,
			hoist: 'all',
			allow: ['resolve', 'reject']
		}
	],
	'no-ternary': DISABLED,
	'no-throw-literal': ERROR,
	'no-undef-init': ERROR,
	'no-undefined': ERROR,
	'no-underscore-dangle': ERROR,
	'no-unneeded-ternary': [ERROR, { defaultAssignment: false }],
	'no-unused-expressions': ERROR,
	'no-useless-call': ERROR,
	'no-useless-computed-key': [ERROR, { enforceForClassMembers: true }],
	'no-useless-concat': ERROR,
	'no-useless-constructor': ERROR,
	'no-useless-rename': ERROR,
	'no-useless-return': ERROR,
	'no-var': ERROR,
	'no-void': ERROR,
	'no-warning-comments': WARN,
	'object-shorthand': [ERROR, 'always', { avoidQuotes: true }],
	'one-var': [ERROR, 'never'],
	'operator-assignment': [ERROR, 'always'],
	'prefer-arrow-callback': ERROR,
	'prefer-const': [
		ERROR,
		{
			destructuring: 'any',
			ignoreReadBeforeAssign: true
		}
	],
	'prefer-destructuring': [
		ERROR,
		{
			VariableDeclarator: {
				array: false,
				object: true
			},
			AssignmentExpression: {
				array: false,
				object: false
			}
		},
		{
			enforceForRenamedProperties: true
		}
	],
	'prefer-exponentiation-operator': ERROR,
	'prefer-named-capture-group': ERROR,
	'prefer-numeric-literals': ERROR,
	'prefer-object-has-own': ERROR,
	'prefer-object-spread': ERROR,
	'prefer-promise-reject-errors': ERROR,
	'prefer-regex-literals': ERROR,
	'prefer-rest-params': ERROR,
	'prefer-spread': ERROR,
	'prefer-template': ERROR,
	radix: [ERROR, 'as-needed'],
	'require-await': ERROR,
	'require-unicode-regexp': WARN,
	'sort-imports': [
		ERROR,
		{
			ignoreCase: true,
			memberSyntaxSortOrder: [
				'none',
				'all',
				'single',
				'multiple'
			]
		}
	],
	'sort-keys': [
		ERROR,
		'asc',
		{
			caseSensitive: true,
			natural: true,
			minKeys: 4
		}
	],
	'sort-vars': ERROR,
	strict: [ERROR, 'never'],
	'symbol-description': ERROR,
	'vars-on-top': ERROR,
	yoda: [ERROR, 'never'],

	// // Layout & Formatting
	'unicode-bom': ERROR
};
