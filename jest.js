import jest from 'eslint-plugin-jest';
import {
	DISABLED,
	ERROR,
	WARN
} from './lib/eslint-enforcement-codes.js';

export default [
	jest.configs['flat/recommended'],
	jest.configs['flat/style'],
	{
		rules: {
			// Disable some base rules
			'no-console': DISABLED,
			'max-classes-per-file': DISABLED,
			'no-undefined': DISABLED,
			'max-nested-callbacks': DISABLED,
			'no-magic-numbers': DISABLED,

			// Jest Rules
			'jest/consistent-test-it': [ERROR, { fn: 'test', withinDescribe: 'test' }],
			'jest/max-expects': DISABLED,
			'jest/max-nested-describe': [ERROR, { max: 5 }],
			'jest/no-conditional-in-test': WARN,
			'jest/no-confusing-set-timeout': ERROR,
			'jest/no-duplicate-hooks': ERROR,
			'jest/no-hooks': DISABLED,
			'jest/no-large-snapshots': ERROR,
			'jest/no-restricted-jest-methods': DISABLED,
			'jest/no-restricted-matchers': DISABLED,
			'jest/no-test-return-statement': ERROR,
			'jest/padding-around-after-all-blocks': ERROR,
			'jest/padding-around-after-each-blocks': ERROR,
			'jest/padding-around-all': ERROR,
			'jest/padding-around-before-all-blocks': ERROR,
			'jest/padding-around-before-each-blocks': ERROR,
			'jest/padding-around-describe-blocks': ERROR,
			'jest/padding-around-test-blocks': ERROR,
			'jest/prefer-called-with': ERROR,
			'jest/prefer-comparison-matcher': ERROR,
			'jest/prefer-each': ERROR,
			'jest/prefer-equality-matcher': ERROR,
			'jest/prefer-expect-assertions': [
				WARN,
				{ onlyFunctionsWithAsyncKeyword: true }
			],
			'jest/prefer-expect-resolves': ERROR,
			'jest/prefer-hooks-in-order': ERROR,
			'jest/prefer-hooks-on-top': ERROR,
			'jest/prefer-importing-jest-globals': DISABLED,
			'jest/prefer-jest-mocked': ERROR,
			'jest/prefer-lowercase-title': DISABLED,
			'jest/prefer-mock-promise-shorthand': ERROR,
			'jest/prefer-snapshot-hint': [ERROR, 'always'],
			'jest/prefer-spy-on': WARN,
			'jest/prefer-strict-equal': WARN,
			'jest/prefer-todo': WARN,
			'jest/require-hook': ERROR,
			'jest/require-to-throw-message': WARN,
			'jest/require-top-level-describe': ERROR,
			'jest/valid-expect': [
				ERROR,
				{
					alwaysAwait: true,
					minArgs: 1,
					maxArgs: 1
				}
			]
		}
	}
];
