import {
	DISABLED,
	ERROR
} from './lib/eslint-enforcement-codes.js';

export default [
	{
		rules: {
			// Disable some rules
			'@typescript-eslint/no-magic-numbers': DISABLED,
			'@typescript-eslint/no-unsafe-member-access': DISABLED,
			'@typescript-eslint/unbound-method': DISABLED,
			'@typescript-eslint/no-non-null-assertion': DISABLED,

			// Jest Rules
			'jest/no-untyped-mock-factory': ERROR,
			'jest/unbound-method': ERROR
		}
	}
];
