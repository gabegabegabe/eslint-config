import { baseRules } from './lib/base-rules.js';
import comments from '@eslint-community/eslint-plugin-eslint-comments/configs';
import js from '@eslint/js';
import promise from 'eslint-plugin-promise';
import {
	DISABLED,
	ERROR,
	WARN
} from './lib/eslint-enforcement-codes.js';

export default [
	js.configs.recommended,
	comments.recommended,
	promise.configs['flat/recommended'],
	{
		rules: {
			// Base Rules
			...baseRules,

			// Promise Rules
			'promise/avoid-new': WARN,
			'promise/no-multiple-resolved': ERROR,
			'promise/no-native': DISABLED,
			'promise/prefer-await-to-callbacks': ERROR,
			'promise/prefer-await-to-then': ERROR,
			'promise/spec-only': ERROR,

			// Eslint Comment Rules
			'@eslint-community/eslint-comments/disable-enable-pair': [
				ERROR,
				{ allowWholeFile: true }
			],
			'@eslint-community/eslint-comments/no-unused-disable': WARN,
			'@eslint-community/eslint-comments/no-use': [
				ERROR,
				{
					allow: [
						'eslint-disable',
						'eslint-disable-next-line',
						'eslint-enable',
						'global'
					]
				}
			]
		}
	}
];
