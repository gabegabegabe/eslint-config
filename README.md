# @gabegabegabe/eslint-config

> Gabe's personal ESLint configs

These are my collection of shareable ESLint configs, with rules tweaked to my
liking.

## Usage
These are intended to be included in your own `eslint.config.js` files, with the
different configs being required when needed.

## Example Config
```js
import base from '@gabegabegabe/eslint-config';
import jest from '@gabegabegabe/eslint-config/jest';
import jestTs from '@gabegabegabe/eslint-config/jest-typescript';
import js from '@gabegabegabe/eslint-config/javascript';
import node from '@gabegabegabe/eslint-config/node';
import ts from '@gabegabegabe/eslint-config/typescript';

export default [
	...base,
	...node,
	...js,
	...ts,
	...jest,
	...jestTs,
	{
		languageOptions: {
			parserOptions: {
				project: './tsconfig.json'
			}
		}
	}
];
```
